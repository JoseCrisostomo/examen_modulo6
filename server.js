const axios = require('axios');

const axiosCacheAdapter = require('axios-cache-adapter');
const config = require('config')

const express = require('express');
const bodyParser = require('body-parser');

const utils = require('./utils');


const logger = createLogger({
    level: 'debug',
    format: combine(
      label({ label: 'main' }), timestamp(),
      myFormat
    ),
    defaultMeta: { service: 'user-service' },
    transports: [
      new transports.File({ filename: 'error.log', level: 'error' }),
      new transports.File({ filename: 'combined.log' }),
      new transports.Console()
    ]
  });

let eventsArray = []

let events = {
    eventName: 'Nombre Del Evento',
    date: 'fecha',
    type: 'tipo',
    place: 'lugar',
    description: 'descripcion',
    key:'99111222'
}

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));

const domainCors = config.get('domainCors');

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", domainCors.join(','));
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});



app.get('/events', (req, res) => {
    const key = req.query['key'];

    if (key.events !== '99111222') {
        res.status(400).send('Faltan parametros');
        return;
    }
})

app.post('/events', (req, res) => {
    const events = req.params['eventName.events','type.events','date.events','place.events','description.events'];

    if (eventName.events === undefined || type.events === undefined || date.events === undefined || place.events === undefined) {
        res.status(404).send('Faltan campos obligatorios');
        return
    } else {
        eventsArray.push(${events});

    }
})

